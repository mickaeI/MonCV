// Menu burger ------------------------------------------------------------------------------------------------------------
var burger = document.querySelector('#burger');
var nav = document.querySelector('nav');
var ombre = document.querySelector('section');
// Déclaration de fonctions
var deroulerMenu = function() {
    nav.classList.toggle("neutre");
    nav.classList.toggle("mobile");
    ombre.classList.toggle("cachee");
    ombre.classList.toggle("visible");
};
// Déclencheurs
burger.addEventListener('click', deroulerMenu);
ombre.addEventListener('click', deroulerMenu);

// Surveillance du scroll ------------------------------------------------------------------------------------------------
var sectionArray = [1, 2, 3, 4, 5];

$.each(sectionArray, function(index, value){  

     $(document).scroll(function(){
         var offsetSection = $('#' + 'section_' + value).offset().top;
         var docScroll = $(document).scrollTop();
         var docScroll1 = docScroll + 1;
         
         if ( docScroll1 >= offsetSection ){
             $('a').removeClass('active');
             $('a:link').addClass('inactive');  
             $('a').eq(index).addClass('active');
             $('a:link').eq(index).removeClass('inactive');
         }
    });
});
$(document).ready(function(){
    $('a:link').addClass('inactive');    
    $('a').eq(0).addClass('active');
    $('a:link').eq(0).removeClass('inactive');
});

// Défilement doux vers les ancres -----------------------------------------------------------------------------------------
$(document).ready(function() {
    $('.js-scrollTo').on('click', function() { // Au clic sur un élément
      var page = $(this).attr('href'); // Page cible
      var speed = 750; // Durée de l'animation (en ms)
      $('html, body').animate( { scrollTop: $(page).offset().top }, speed ); // Go
      return false;
    });
  });

